pipeline {
    agent any
    environment {
        BITBUCKET_CREDS             = 'svc_jenkins'
        GIT_BRANCH                  = 'release/maintest2'
        GIT_REPO_URL                = 'git@bitbucket.org:americanfirstfinance/smoketests.git'
        WORKSPACE                   = pwd()
    }
    options {
        buildDiscarder(logRotator(numToKeepStr:'10', artifactNumToKeepStr:'10'))
        skipDefaultCheckout()
        disableConcurrentBuilds()
    }
    stages {
        stage('CleanWorkspace') {
            steps {
                cleanWs()
                dir("${env.WORKSPACE}@tmp") {
                    deleteDir()
                }
                dir("${env.WORKSPACE}@script") {
                    deleteDir()
                }
                dir("${env.WORKSPACE}@script@tmp") {
                    deleteDir()
                }
                step([$class:'WsCleanup'])
            }
        }
        stage('Checkout') {
            steps {
                git branch:"${GIT_BRANCH}", credentialsId:"${env.BITBUCKET_CREDS}", url:"${env.GIT_REPO_URL}"
            }
        }
        stage('Sonarqube') {
            environment {
                scannerHome = tool name: 'SonarQubeScanner' , type: 'hudson.plugins.sonar.SonarRunnerInstallation'
            }
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh "${scannerHome}/bin/sonar-scanner -Dsonar.branch.name=$GIT_BRANCH"
                }
            }
        }
        stage('Quality gate') {
            steps {
                timeout(time: 1, unit: 'HOURS') { // Just in case something goes wrong, pipeline will be killed after a timeout
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage("Polaris Analysis"){
            steps {
                slackSend (color: '#FFFF00', message: "Polaris Analysis STARTED")
                script {
                    def polarisStatus = polaris arguments: 'analyze -w --incremental $CHANGE_SET_FILE_PATH', createChangeSetFile: [excluding: '', including: '*.java,*.php,*.js', returnSkipCode: true], polarisCli: 'Polaris CLI', returnStatus: true
                    if (polarisStatus == -1) {
                    print 'Incremental analysis was skipped because no change set file could be created. Falling back on full analysis'
                    polarisStatus = polaris arguments: 'analyze -w', polarisCli: 'Polaris CLI', returnStatus: true
                    }

                    if (polarisStatus == 0) {
                        slackSend (color: '#00FF00', message: "Polaris Analysis done successfully")
                        print 'Success: Polaris static analysis succeeded, perform issue check'
                        polarisIssueCheck()
                    } else {
                        slackSend (color: '#FF0000', message: "Polaris Analysis failed")
                        print 'Failure: Both incremental analysis and full analysis failed'
                    }
                }
            }
        }
        stage('Polaris blackduck') {
            steps {
                //sh'''sudo chmod -R 777 /home/jenkins/workspace/dev-C2SIntegration-Service'''
                script {
                    synopsys_detect detectProperties: ''' 
                    --blackduck.trust.cert=true --detect.project.version.phase=RELEASED --detect.project.version.distribution=EXTERNAL --detect.policy.check.fail.on.severities=CRITICAL --detect.timeout=300 --detect.detector.buildless=true
                    ''', downloadStrategyOverride: [$class: 'ScriptOrJarDownloadStrategy']
                }
            }
        } 
        stage("Maven Build") {
            steps {
                script {
                    sh '''
                    mvn clean install 
                    mvn test -Dbrowser=grid-chrome -Denv=test2
                    '''
                }
            }
        }
    }
}