package com.aff.pages.consumerportal;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;
import com.aff.utilities.Utils;

public class CPHomePage extends BasePage{
	
	@FindBy(css = ".profile-inner>img")
	private WebElement profileArrow;
	
	@FindBy(xpath = "//div[@class='ml-5 pr-10 col']//div[@class='logout-link']//a[@href='#'][normalize-space()='Log Out']")
	private WebElement logOut;
	
	public CPHomePage(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static CPHomePage using(WebDriver driver) throws IOException {
        return new CPHomePage(driver);
    }
	
	public void logoutUser() {
		browserWait=5;
		this.profileArrow.click();
		this.logOut.click();
		Utils.pauseExecution(browserWait);
		}

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_CPHOME_PAGE;
		
	}

}
