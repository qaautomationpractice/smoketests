package com.aff.pages.consumerportal;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;
import com.aff.utilities.Utils;

public class CPLoginPage extends BasePage{
	
	@FindBy(id = "input-21")
	private WebElement userName;
	
	@FindBy(id = "input-24")
	private WebElement ssn;
	
	@FindBy(xpath = "//button[@type='submit']")
	private WebElement loginButton;
	
	@FindBy(id = "input-46")
	private WebElement verificationCode;
	
	@FindBy(css = ".profile-inner")
	private WebElement profile;
	
	

	public CPLoginPage(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static CPLoginPage using(WebDriver driver) throws IOException {
        return new CPLoginPage(driver);
    }
	
	
	public void launchCP() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.CP_URL));
	}
	
	public void loginUser() {
		explicitWait(this.loginButton,waitTime,"visibility");
		this.userName.clear();
		this.userName.sendKeys(appPropertiesEnvironment.getProperty("CP_UserID"));
		this.ssn.sendKeys(appPropertiesEnvironment.getProperty("CP_Password"));
		this.loginButton.click();
		Utils.pauseExecution(browserWait);
		this.verificationCode.sendKeys(appPropertiesEnvironment.getProperty("CP_VerificationCode"));
		this.loginButton.click();
		Utils.pauseExecution(browserWait);
	}
	
	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_CPLOGIN_PAGE;
		
	}

}
