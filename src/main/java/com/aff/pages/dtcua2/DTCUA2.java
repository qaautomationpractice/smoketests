package com.aff.pages.dtcua2;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class DTCUA2 extends BasePage{
	@FindBy(xpath = "//span[@class='v-btn__content']")
	private WebElement startApplication;
	
	public DTCUA2(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static DTCUA2 using(WebDriver driver) throws IOException {
        return new DTCUA2(driver);
    }
	
	public void launchDTCUA2() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.DTCUA2_URL));
	}
	
	public void validatePageIsLaunched() {
		String name = "Start Application";
		Assert.assertEquals(getTrimmedText(this.startApplication.getText()), name);
		     
		
	}


	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_DTCUA2_PAGE;
		
	}

}
