package com.aff.pages.dtcua1;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class DTCUA1 extends BasePage  {
	
	@FindBy(xpath = "//a[normalize-space()='Continue Application']")
	private WebElement continueApplication;
	
	public DTCUA1(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static DTCUA1 using(WebDriver driver) throws IOException {
        return new DTCUA1(driver);
    }
	
	public void launchDTCUA1() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.DTCUA1_URL));
	}
	
	public void validatePageIsLaunched() {
		String name = "CONTINUE APPLICATION";
		Assert.assertEquals(getTrimmedText(this.continueApplication.getText()), name);
		     
		
	}

	@Override
	public void validatePageTitle() {
		SoftAssert softAssertion= new SoftAssert();
		softAssertion.assertEquals(getTitle(), getExpectedTitle());
	}

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_DTCUA1_PAGE;
		
	}

}
