package com.aff.pages.rentadvanceua1;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class RentAdvanceUA1 extends BasePage {
	@FindBy(xpath = "//div[@class='primary-content']//h1")
	private WebElement pageHeader;
	
	public RentAdvanceUA1(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static RentAdvanceUA1 using(WebDriver driver) throws IOException {
        return new RentAdvanceUA1(driver);
    }
	
	public void launchRAUA1() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.RAUA1_URL));
	}
	
	public void validatePageIsLaunched() {
		String name = "Rent Paid on Time. Every Time.";
		Assert.assertEquals(getTrimmedText(this.pageHeader.getText()), name);
		     
		
	}

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_RAUA1_PAGE;
		
	}

}
