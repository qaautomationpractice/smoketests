package com.aff.pages.leadgenua1;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class LeadGenUA1 extends BasePage{
	@FindBy(xpath = "//a[@href='/continue-application']")
	private WebElement continueApplication;
	
	public LeadGenUA1(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static LeadGenUA1 using(WebDriver driver) throws IOException {
        return new LeadGenUA1(driver);
    }
	
	public void launchLGUA1() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.LEADGENUA1_URL));
	}
	
	public void validatePageIsLaunched() {
		String name = "CONTINUE APPLICATION";
		Assert.assertEquals(getTrimmedText(this.continueApplication.getText()), name);
	}


	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_LEADGENUA1_PAGE;
		
	}

}
