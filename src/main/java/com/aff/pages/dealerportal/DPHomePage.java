package com.aff.pages.dealerportal;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class DPHomePage extends BasePage {
	
	@FindBy(xpath = "//div[@id='app']//h3")
	private WebElement pageHeaderText;
	
	@FindBy(xpath = "//img[@class='profile-icon']")
	private WebElement profileIcon;
	
	@FindBy(xpath = "//a[@class='expand-profile']")
	private WebElement logout;
	
	@FindBy(xpath = "//a[normalize-space()='Submit App']")
	private WebElement submitAppMenu;
	
	@FindBy(xpath = "//h4[@class='page-desc-text']")
	private WebElement submitAppPageHeader;
	
	@FindBy(xpath = "//a[contains(text(),'Reports')]")
	private WebElement reportsMenu;
	
	@FindBy(xpath = "//h4[@class='form-title']")
	private WebElement reportsPageHeader;
	
	@FindBy(xpath = "//a[contains(text(),'Calculator')]")
	private WebElement calculatorMenu;
	
	@FindBy(xpath = "//div[@class='card-title aff-white']//h5")
	private WebElement calculatorPageHeader;
	
	@FindBy(xpath = "//a[contains(text(),'Training')]")
	private WebElement trainingMenu;
	
	@FindBy(xpath = "//div[@class='wrapper']/div[1]/strong")
	private WebElement trainingPageHeader;
	
	@FindBy(xpath = "//a[contains(text(),'Live Chat')]")
	private WebElement liveChatMenu;
	
	@FindBy(xpath = "//div[@id='designstudio']")
	private WebElement liveChatFrame;
	
	private String visibility = "visibility";
	
	public DPHomePage(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static DPHomePage using(WebDriver driver) throws IOException {
        return new DPHomePage(driver);
    }
	
	public void logoutUser() {
		this.profileIcon.click();
		this.logout.click();
		}
	
	public void validatePageHeader() {
		validateHeaders(this.pageHeaderText.getText(), this.headerText);
		}
	
	public void navigateToSubmitAppPage() {
		this.submitAppMenu.click();
		explicitWait(this.submitAppPageHeader,explicitWaitTime,visibility);
	}
	public void validateSubmitAppPageHeader() {
		validateHeaders(this.submitAppPageHeader.getText(),ApplicationConstants.PAGEHEADER_DPSUBMITAPPPAGE);
	}
	
	public void navigateToReportsPage() {
		this.reportsMenu.click();
		explicitWait(this.reportsPageHeader,explicitWaitTime,visibility);
	}
	public void validateReportsPageHeader() {
		validateHeaders(this.reportsPageHeader.getText(),ApplicationConstants.PAGEHEADER_DPREPORTSPAGE);
	}
	
	public void navigateToCalculatorPage() {
		this.calculatorMenu.click();
		explicitWait(this.calculatorPageHeader,explicitWaitTime,visibility);
	}
	public void validateCalculatorPageHeader() {
		checkHeaderContainsText(this.calculatorPageHeader.getText(),ApplicationConstants.PAGEHEADER_DPCALCULATORPAGE);
	}
	
	public void navigateToTrainingPage() {
		this.trainingMenu.click();
		explicitWait(this.trainingPageHeader,explicitWaitTime,visibility);
	}
	public void validateTrainingPageHeader() {
		validateHeaders(this.trainingPageHeader.getText(),ApplicationConstants.PAGEHEADER_DPTRAININGPAGE);
	}
	
	public void navigateToLiveChatPage() {
		this.liveChatMenu.click();
	}
	
	public void validateLiveChatFrame() {
		System.out.println("live chat class attr: "+this.liveChatFrame.getAttribute("class"));
		Assert.assertNotNull(this.liveChatFrame.getAttribute("class"),"checking if the live chat frame is displayed");
	}
	
	

	@Override
	protected void setPageproperties() {
		this.headerText = ApplicationConstants.PAGEHEADER_DPHOMEPAGE;
		
		
	}

}
