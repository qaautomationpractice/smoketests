package com.aff.pages.dealerportal;

import java.io.IOException;
import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;
import com.aff.utilities.Utils;

public class DPLoginPage extends BasePage {
	
	@FindBy(xpath = "//input[@class='Entityid merchant-input-pos']")
	private WebElement entityID;
	
	@FindBy(xpath = "//input[@class='username merchant-input-pos']")
	private WebElement userName;
	
	@FindBy(xpath = "//input[@type='password']")
	private WebElement password;
	
	@FindBy(xpath = "//button[@class='showButton']")
	private WebElement loginButton;
	
	
	
	
	
	
	public DPLoginPage(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static DPLoginPage using(WebDriver driver) throws IOException {
        return new DPLoginPage(driver);
    }
	
	public void launchDP() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.DP_URL));
		waitTillPageLoad();
	}
	
	public void enterCredentials() {
		explicitWait(entityID,waitTime,"visibility");
		String id = appPropertiesEnvironment.getProperty("DP_entityId");
		String name = appPropertiesEnvironment.getProperty("DP_userName");
		String pwd = appPropertiesEnvironment.getProperty("DP_password");
		this.entityID.sendKeys(id);
		this.userName.sendKeys(name);
		this.password.sendKeys(pwd);	
	}
	
	public void clickLoginButton() {
		this.loginButton.click();
		Utils.pauseExecution(browserWait);
	}
	
	
	

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_DPLOGIN_PAGE;
		
	}
	
	
	

}
