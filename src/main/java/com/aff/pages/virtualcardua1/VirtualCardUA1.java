package com.aff.pages.virtualcardua1;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;


public class VirtualCardUA1 extends BasePage{
	@FindBy(xpath = "//a[@href='/authentication']")
	private WebElement continueApplication;
	
	public VirtualCardUA1(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static VirtualCardUA1 using(WebDriver driver) throws IOException {
        return new VirtualCardUA1(driver);
    }
	
	public void launchVCUA1() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.VCUA1_URL));
	}
	
	public void validatePageIsLaunched() {
		String name = "CONTINUE APPLICATION";
		Assert.assertEquals(getTrimmedText(this.continueApplication.getText()), name);
	}

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_VCUA1_PAGE;
		
	}

}
