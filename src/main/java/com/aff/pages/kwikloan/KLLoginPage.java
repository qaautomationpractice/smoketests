package com.aff.pages.kwikloan;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class KLLoginPage  extends BasePage {
	
	@FindBy(id = "userid")
	private WebElement userID;
	
	@FindBy(id = "password")
	private WebElement password;
	
	@FindBy(xpath = "//button[@class='newBtn newBtnPrimary']")
	private WebElement loginButton;
	
	
	public KLLoginPage(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static KLLoginPage using(WebDriver driver) throws IOException {
        return new KLLoginPage(driver);
    }
	
	public void launchKLPage() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.KL_URL));
	}
	
	public void enterCredetnials() {
		this.userID.sendKeys(appPropertiesEnvironment.getProperty("KL_UserID"));
		this.password.sendKeys(appPropertiesEnvironment.getProperty("KL_Password"));
	}
	
	public void clickLoginButton() {
		this.loginButton.click();
	}

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_KLLOGIN_PAGE;
		
	}

}
