package com.aff.pages.kwikloan;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class KLHomePage  extends BasePage {
	
	@FindBy(xpath = "//div[@id='userCont']//div/i")
	private WebElement signout;
	
	@FindBy(id = "navigation")
	private WebElement navigationFrame;
	
	public KLHomePage(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static KLHomePage using(WebDriver driver) throws IOException {
        return new KLHomePage(driver);
    }
	
	
	
	public void signoutUser() {
		switchToFrame(this.navigationFrame);
		String clickable = "clickable";
		explicitWait(this.signout,5,clickable);
		this.signout.click();
	}
	

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_KLHOME_PAGE;
		
	}

}
