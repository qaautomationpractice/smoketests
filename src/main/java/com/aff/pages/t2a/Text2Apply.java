package com.aff.pages.t2a;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aff.baseweb.BasePage;
import com.aff.constants.ApplicationConstants;

public class Text2Apply extends BasePage{
	
	@FindBy(xpath = "//h1[@class='merchant_name']")
	private WebElement pageHeader;
	
	public Text2Apply(WebDriver driver) throws IOException {
		super(driver);
	}
	
	public static Text2Apply using(WebDriver driver) throws IOException {
        return new Text2Apply(driver);
    }
	
	public void launchT2A() {
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(appPropertiesEnvironment.getProperty(ApplicationConstants.T2A_URL));
	}
	
	public void validatePageIsLaunched() {
		String name = "Shop Our Partners";
		Assert.assertEquals(getTrimmedText(this.pageHeader.getText()), name);
	}

	@Override
	protected void setPageproperties() {
		this.pageTitle = ApplicationConstants.TITLE_T2A_PAGE;
		
	}

}
