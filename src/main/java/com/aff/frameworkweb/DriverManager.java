package com.aff.frameworkweb;

import java.io.IOException;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.aff.utilities.ApplicationProperties;

public abstract class DriverManager {

	protected RemoteWebDriver instance;
	protected ApplicationProperties appPropertiesWeb;

	// factory method to create driver instance
	protected abstract void createDriver();
	
	public String getSauceLabURL() throws IOException {
		appPropertiesWeb = ApplicationProperties.getInstance("web");
   	 	String username = appPropertiesWeb.getProperty("SAUCE_USERNAME");
        String accessKey = appPropertiesWeb.getProperty("SAUCE_ACCESS_KEY");
        String sauce_LabsURL = "https://" + username+ ":" + accessKey + "@ondemand.saucelabs.com:443/wd/hub";
   	
		return sauce_LabsURL;
	}

	public RemoteWebDriver getDriver() {
		if (instance == null) {
			synchronized (DriverManager.class) {
				if (instance == null) {
					createDriver();
				}
			}
		}
		return instance;
	}

	public void quit() {
		if (instance != null) {
			instance.quit();
			instance = null;
		}
	}
}
