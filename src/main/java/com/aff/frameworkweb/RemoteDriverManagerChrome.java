package com.aff.frameworkweb;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class RemoteDriverManagerChrome extends DriverManager {


    @Override
    protected void createDriver() {
    	 ChromeOptions chromeOptions = new ChromeOptions();
         chromeOptions.setCapability("browserVersion", "latest");
         chromeOptions.setCapability("platformName", "Windows 10");

        try {
            instance = new RemoteWebDriver(new URL(getSauceLabURL()), chromeOptions);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException i) {
			i.printStackTrace();
		}
    }
}
