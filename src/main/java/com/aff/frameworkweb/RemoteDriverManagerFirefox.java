package com.aff.frameworkweb;

import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class RemoteDriverManagerFirefox extends DriverManager {

    @Override
    protected void createDriver() {
    	FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability("browserVersion", "latest");
        firefoxOptions.setCapability("platformName", "Windows 10");
        

        try {
            instance = new RemoteWebDriver(new URL(getSauceLabURL()), firefoxOptions);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException i) {
			i.printStackTrace();
		}
    }
}
