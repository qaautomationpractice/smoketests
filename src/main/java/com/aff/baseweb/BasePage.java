package com.aff.baseweb;

import java.io.IOException;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aff.constants.ApplicationConstants;
import com.aff.pages.common.PageHeader;
import com.aff.utilities.ApplicationProperties;

public abstract class BasePage {

	protected WebDriver driver;
	protected PageHeader header;

	protected String pageTitle;
	protected String pageUrl;
	protected String headerText;
	protected long pollTime;
	protected long waitTime;
	protected long explicitWaitTime;
	protected int browserWait;
	protected ApplicationProperties appPropertiesEnvironment;
	protected ApplicationProperties appPropertiesWeb;

	protected BasePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		header = PageHeader.getInstance(this.driver);
		appPropertiesEnvironment = ApplicationProperties.getEnvironment();
		try {
			appPropertiesWeb = ApplicationProperties.getInstance("web");
		} catch (IOException e) {
			e.printStackTrace();
		}
		pollTime = Integer.parseInt(appPropertiesWeb.getProperty(ApplicationConstants.POLL_TIME));
		waitTime =Integer.parseInt(appPropertiesWeb.getProperty(ApplicationConstants.PAGE_LOAD_WAIT_TIME));
		browserWait = Integer.parseInt(appPropertiesWeb.getProperty(ApplicationConstants.BROWSER_PAUSE));
		explicitWaitTime = Integer.parseInt(appPropertiesWeb.getProperty(ApplicationConstants.IMPLICIT_WAIT_TIME));
		// defaults to title strategy if not set by setPageproperties
		setPageproperties();
	}

	protected abstract void setPageproperties();

   public void switchToFrame(WebElement ele) {
	   this.driver.switchTo().frame(ele);
   }

	public String getTitle() {
		return driver.getTitle();
	}

	public String getExpectedTitle() {
		return this.pageTitle;
	}

	public String getURL() {
		return driver.getCurrentUrl();
	}

	public String getPageURL() {
		return this.pageUrl;
	}

	public PageHeader pageHeader() {
		return header;
	}
	
	public void validatePageTitle() {
		Assert.assertEquals(getTitle(), getExpectedTitle());
	}
	
	public void validateHeaders(String actualHeader,String expectedHeader) {
		Assert.assertEquals(actualHeader.trim(), expectedHeader);
	}
	public void checkHeaderContainsText(String actualHeader,String expectedHeader) {
		Assert.assertTrue(actualHeader.contains(expectedHeader));
	}
	
	public String getTrimmedText(String s) {
		return s.trim();
	}
	

	protected boolean isElementPresent(By by) {
		return !driver.findElements(by).isEmpty();
	}
	
	 public void scrollToElementWithJS(WebElement element) {
	        try {
	            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	        } catch (Exception e) {
	            Assert.fail("Failed to scroll with exception " + e);
	        }
	    }
	 
	    public void clickElementWithJS(WebElement element) {
	        try {
	            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
	        } catch (Exception e) {
	            Assert.fail("Failed to click with exception " + e);
	        }
	    }
	    
	    public void waitTillPageLoad() {
	    	try {
	    		new WebDriverWait(driver, Duration.ofSeconds(waitTime)).until(
	    			      webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
	        } catch (Exception e) {
	            Assert.fail("Failed to wait till the page loaded " + e);
	        }
	    	
	    }
	    
	    public void explicitWait(WebElement element, long time, String condition) {
	        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(time));
	        switch (condition) {
	            case "visibility":
	                wait.until(ExpectedConditions.visibilityOf(element));
	                break;
	            case "clickable":
	                wait.until(ExpectedConditions.elementToBeClickable(element));
	                break;
	            case "selected":
	                wait.until(ExpectedConditions.elementToBeSelected(element));
	                break;
	            case "visibilityOfElementLocated":
	                wait.until(ExpectedConditions.visibilityOfElementLocated((By) element));
	                break;
	            default:
	            	wait.until(ExpectedConditions.presenceOfElementLocated((By) element));
	        }
	    }
	
	protected void waitForElementClickable(long timeout, final WebElement element) {
		new WebDriverWait(driver, Duration.ofSeconds(timeout)).pollingEvery(Duration.ofSeconds(pollTime))
				.until(ExpectedConditions.elementToBeClickable(element));
	}

	protected void waitForElementToBeVisible(long timeout, final WebElement element) {
		new WebDriverWait(driver, Duration.ofSeconds(timeout)).pollingEvery(Duration.ofSeconds(pollTime))
				.until(ExpectedConditions.visibilityOf(element));
	}
	
	protected void waitForDuration(long time) {
		this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(time));
	}

	
	
}
