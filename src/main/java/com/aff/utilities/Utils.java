package com.aff.utilities;

import com.aff.exceptions.TAFRuntimeException;

public class Utils {
	private Utils(){
	}
	public static void pauseExecution(int seconds){
		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new TAFRuntimeException(e);
		}
	}
}
