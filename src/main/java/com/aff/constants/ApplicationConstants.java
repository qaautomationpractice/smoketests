package com.aff.constants;

public class ApplicationConstants {
	private ApplicationConstants(){
	}
	public static final String CONFIG_FILE = "web.properties";
	public static final String BROWSER_NAME = "browser";
	public static final String BROWSER_NAME_FF = "browserFF";
	public static final String IMPLICIT_WAIT_TIME = "waitime";
	public static final String BROWSER_PAUSE = "browserwait";
	public static final String PAGE_LOAD_WAIT_TIME = "pagewait";
	public static final String POLL_TIME = "pollTime";
	public static final String WEBTESTS = "web";
	public static final String ENVIRONMENT = "env";
	
	public static final String REST_URI = "restAssuredURL";
	public static final String REST_ASSURED_USER_PATH = "restAssuredUsersPath";	

		
	public static final String TITLE_HOME_PAGE = "Demo Web Shop";
	public static final String TITLE_LOGIN_PAGE = "Demo Web Shop. Login";
	
	public static final String URL_HOME_PAGE = "http://demowebshop.tricentis.com/";
	public static final String URL_LOGIN_PAGE = "http://demowebshop.tricentis.com/login";
	
	public static final String PAGEHEADER_DPHOMEPAGE = "LTO Test Dealer";
	public static final String TITLE_DPLOGIN_PAGE = "Dealer Portal";
	public static final String DP_URL = "dealerPortalUrl";
	public static final String PAGEHEADER_DPSUBMITAPPPAGE = "Choose State And Location To Proceed Further";
	public static final String PAGEHEADER_DPREPORTSPAGE = "Please Select A Location To View Reports With American First Finance";
	public static final String PAGEHEADER_DPCALCULATORPAGE = "Bi-Weekly Payments";
	public static final String PAGEHEADER_DPTRAININGPAGE = "Spotlight Trainings";
	//Kwikloan 
	public static final String KL_URL = "kwikLoanUrl";
	public static final String TITLE_KLLOGIN_PAGE = "Main Login Program";
	public static final String TITLE_KLHOME_PAGE = "Kwik-Loan Main Page";
	
	//Customer portal
	public static final String CP_URL = "consumerPortalUrl";
	public static final String TITLE_CPLOGIN_PAGE = "Customer Login";
	public static final String TITLE_CPHOME_PAGE = "Accounts";
	
	//DTCUA1
	public static final String DTCUA1_URL = "dtcUa1Url";
	public static final String TITLE_DTCUA1_PAGE = "\\u00A0|\\u00A0American First Finance";
	
	//DTCUA2
	public static final String DTCUA2_URL = "dtcUa2Url";
	public static final String TITLE_DTCUA2_PAGE = "Apply with American First Finance";
	
	//Virtual card ua1
	public static final String VCUA1_URL = "virtualCardUrl";
	public static final String TITLE_VCUA1_PAGE = "\u00A0|\u00A0American First Finance";
	
	//Rent advance ua1
	public static final String RAUA1_URL = "rentAdvanceUrl";
	public static final String TITLE_RAUA1_PAGE = "Home\u00A0|\u00A0Rent Advance";
	
	//Virtual card ecomm ua1
	public static final String VCECOMMUA1_URL = "virtualCardEcomURl";
	public static final String TITLE_VCECOMMUA1_PAGE = "\u00A0|\u00A0American First Finance";
	
	//Lead gen ua1
	public static final String LEADGENUA1_URL = "leadGenUrl";
	public static final String TITLE_LEADGENUA1_PAGE = "Application | Easy & Flexible Cash Loans";
	
	//text2apply 
	public static final String T2A_URL = "text2applyUrl";
	public static final String TITLE_T2A_PAGE = "Find Locations";
	
	
	
}
