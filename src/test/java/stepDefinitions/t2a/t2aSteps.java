package stepDefinitions.t2a;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.t2a.Text2Apply;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepDefinitions.ContextSteps;

public class t2aSteps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public t2aSteps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I launch T2A application")
	public void i_launch_t2a_application() throws IOException {
		Text2Apply.using(driver).launchT2A();
		Text2Apply.using(driver).validatePageTitle();
		logger.info("the text 2 apply portal is successfully launched");
	}

	@Then("I verify that the T2A page is launched without issues")
	public void i_verify_that_the_t2a_page_is_launched_without_issues() throws IOException {
		Text2Apply.using(driver).validatePageIsLaunched();
		logger.info("An element on text 2 apply is validated with its text");
	   
	}

}
