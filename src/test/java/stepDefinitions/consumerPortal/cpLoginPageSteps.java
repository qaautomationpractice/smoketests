package stepDefinitions.consumerPortal;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.consumerportal.CPHomePage;
import com.aff.pages.consumerportal.CPLoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinitions.ContextSteps;

public class cpLoginPageSteps {
	
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public cpLoginPageSteps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I am on the consumer portal login page")
	public void i_am_on_the_consumer_portal_login_page() throws IOException {
		CPLoginPage.using(driver).launchCP();
		CPLoginPage.using(driver).validatePageTitle();
		logger.info("the user is on login page for consumer portal");
		
	    
	}

	@When("I enter the credentials and click on submit for the customer user")
	public void i_enter_the_credentials_and_click_on_submit_for_the_customer_user() throws IOException {
		CPLoginPage.using(driver).loginUser();
		logger.info("the user signed into consumer portal");
		
	    
	}

	@Then("the user is successfully logged into consumer portal")
	public void the_user_is_successfully_logged_into_consumer_portal() throws IOException {
		CPHomePage.using(driver).validatePageTitle();
		logger.info("the user is on home page of consumer portal");
	}

}
