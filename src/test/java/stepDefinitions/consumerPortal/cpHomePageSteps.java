package stepDefinitions.consumerPortal;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.consumerportal.CPHomePage;
import com.aff.pages.consumerportal.CPLoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinitions.ContextSteps;

public class cpHomePageSteps {
	
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public cpHomePageSteps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I am on home page for consumer portal")
	public void i_am_on_home_page_for_consumer_portal() throws IOException {
		CPLoginPage.using(driver).launchCP();
		CPLoginPage.using(driver).loginUser();
		CPHomePage.using(driver).validatePageTitle();
		logger.info("the user is on home page of consumer portal");
	    
	}

	@When("I click on consumer portal logout button")
	public void i_click_on_consumer_portal_logout_button() throws IOException {
		CPHomePage.using(driver).logoutUser();
		logger.info("the user tried logging out of consumer portal");
	    
	}

	@Then("the logout is successful from consumer portal")
	public void the_logout_is_successful_from_consumer_portal() throws IOException {
	    CPLoginPage.using(driver).validatePageTitle();
	    logger.info("the logout is successful out of consumer portal");
	}

}
