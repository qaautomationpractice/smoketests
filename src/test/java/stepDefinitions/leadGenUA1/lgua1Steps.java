package stepDefinitions.leadGenUA1;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.leadgenua1.LeadGenUA1;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepDefinitions.ContextSteps;

public class lgua1Steps {

	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public lgua1Steps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I launch LGUA1 application")
	public void i_launch_lgua1_application() throws IOException {
		LeadGenUA1.using(driver).launchLGUA1();
		LeadGenUA1.using(driver).validatePageTitle();
		logger.info("the Lead Gen UA1 portal is successfully launched");
	    
	}

	@Then("I verify that the LGUA1 page is launched without issues")
	public void i_verify_that_the_lgua1_page_is_launched_without_issues() throws IOException {
		LeadGenUA1.using(driver).validatePageIsLaunched();
		logger.info("An element on Lead Gen UA1 is validated with its text");
	   
	}
	
}
