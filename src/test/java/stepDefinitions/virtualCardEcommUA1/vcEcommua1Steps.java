package stepDefinitions.virtualCardEcommUA1;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.virtualcardecommua1.VirtualCardEcommUA1;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepDefinitions.ContextSteps;

public class vcEcommua1Steps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public vcEcommua1Steps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I launch VCECOMMUA1 application")
	public void i_launch_vcecommua1_application() throws IOException {
		VirtualCardEcommUA1.using(driver).launchVCUA1();
		VirtualCardEcommUA1.using(driver).validatePageTitle();
		logger.info("the virtual card ecomm UA1 portal is successfully launched");
	}

	@Then("I verify that the VCECOMMUA1 page is launched without issues")
	public void i_verify_that_the_vcecommua1_page_is_launched_without_issues() throws IOException {
		VirtualCardEcommUA1.using(driver).validatePageIsLaunched();
		logger.info("An element on virtual card ecomm UA1 is validated with its text");
	    
	}

}
