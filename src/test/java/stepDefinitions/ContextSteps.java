package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.JavascriptExecutor;

import com.aff.constants.ApplicationConstants;
import com.aff.frameworkweb.DriverManager;
import com.aff.frameworkweb.DriverManagerFactory;
import com.aff.frameworkweb.DriverType;
import com.aff.utilities.ApplicationProperties;
import com.aff.utilities.LoggerProperties;
import com.aff.data.TestData;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.AfterAll;

public class ContextSteps  {
	protected ApplicationProperties appPropertiesWeb,appPropertiesApi,appPropertiesEnvironment;
    private   WebDriver driver;
    protected DriverManager driverManager;
    protected DriverType driverType;
    protected LoggerProperties loggerProperties;
    private static Logger logger;
    static int passCount,failCount = 0;
    public static final String TOKEN = "xoxb-13152307187-2974148076642-6BRmzmux2Gs9mocgHsesZ4Su";
    public static final String CHANNEL = "#offshore_automation";
    
   
    public WebDriver getDriver() {
        return driver;
     }
    
    @Before(order=1)
   	public void setup(Scenario scenario) throws IOException {
       	loggerProperties = LoggerProperties.getInstance();
           setLogger(loggerProperties.getLogger());
       	String browser = System.getProperty("browser");
       	String environment = System.getProperty("env"); 
       	appPropertiesWeb = ApplicationProperties.getInstance("web");
       	appPropertiesApi = ApplicationProperties.getInstance("api");
       	if(browser == null) {
       		browser = appPropertiesWeb.getProperty("browser");
       	}
       	if(environment == null) {
       		environment = appPropertiesWeb.getProperty("env");
       	}
       	appPropertiesEnvironment = ApplicationProperties.getEnvironment(environment);
   		  setBrowserType(browser);
   			driverManager = DriverManagerFactory.getDriverManager(driverType);
   			driver = driverManager.getDriver();	
   			if(browser.contains("grid")) {
   			((JavascriptExecutor)driver).executeScript("sauce:job-name="+scenario.getName());
               ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
   			}
   	}


    @Before(order=2)
    public void configureWaitTimes() {
        int implicitWaitTime = Integer.parseInt(appPropertiesWeb.getProperty(ApplicationConstants.IMPLICIT_WAIT_TIME));
        long pageloadWaitTime = Integer.parseInt(appPropertiesWeb.getProperty(ApplicationConstants.PAGE_LOAD_WAIT_TIME));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(implicitWaitTime));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(pageloadWaitTime));
    }

    @Before(order=3)
    public void maximizeBrowser() {
        driver.manage().window().maximize();
    }
    @Before(order=4)
    public void openStartURL() {
    	long waitTime = 30;
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		driver.get(TestData.START_URL);	
	}
    
    @AfterStep
	public void addScreenshot(Scenario scenario) throws IOException {
		  File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		  byte[] fileContent = FileUtils.readFileToByteArray(screenshot);
		  scenario.attach(fileContent, "image/png", "screenshot");
		
	}
    

    @After
    public void stopBrowser(Scenario scenario) {
    	if(scenario.isFailed()) {
    		failCount+=1;
    	}else {
    		passCount+=1;
    	}
        driverManager.quit();
    }
    
    @AfterAll
    public static void before_or_after_all() {
//    	ChatPostMessageResponse response = null;
//    	SlackMessageHelper slackMessage = new SlackMessageHelper();
//        String message = "Smoke tests executed successfully. Please analyze the reports";
//        String message = "[<" + message + ">] " +\n+"total tests :"+(passCount+failCount)+"fail count :"+failCount;
//        try {
//        	response = slackMessage.sendMessage(CHANNEL, message,TOKEN);
//        	logger.info("the slack response is:"+response);
//        } catch (IOException e) {
//            e.printStackTrace();
//            logger.error("sending slack message failed"+response);
//        }
    	
    }
    
    
     
    
    protected void setBrowserType(String browserString) {
     	if(browserString.equals(null)) {
     		browserString = appPropertiesWeb.getProperty(ApplicationConstants.BROWSER_NAME);
     	}
         if (browserString.equalsIgnoreCase("chrome")) {
             driverType = DriverType.CHROME;
         } else if (browserString.contains("firefox")) {
             driverType = DriverType.FIREFOX;
         } else if (browserString.equalsIgnoreCase("grid-chrome")) {
             driverType = DriverType.GRID_CHROME;
         } else if (browserString.equalsIgnoreCase("grid-firefox")) {
             driverType = DriverType.GRID_FIREFOX;
         }else {
             throw new AssertionError("Unsupported Browser");
         }
     }

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		ContextSteps.logger = logger;
	}


}
