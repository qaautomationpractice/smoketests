package stepDefinitions.dtcua2;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.dtcua2.DTCUA2;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepDefinitions.ContextSteps;

public class dtcua2Steps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public dtcua2Steps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I launch DTCUA2 application")
	public void i_launch_dtcua2_application() throws IOException {
		DTCUA2.using(driver).launchDTCUA2();
		DTCUA2.using(driver).validatePageTitle();
		logger.info("the DTC UA2 portal is successfully launched");
	   
	}

	@Then("I verify that the dtcua2 page is launched without issues")
	public void i_verify_that_the_dtcua2_page_is_launched_without_issues() throws IOException {
		DTCUA2.using(driver).validatePageIsLaunched();
		logger.info("An element on DTC UA2 is validated with its text");
	    
	}


}
