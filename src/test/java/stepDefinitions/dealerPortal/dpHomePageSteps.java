package stepDefinitions.dealerPortal;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.aff.pages.dealerportal.DPHomePage;
import com.aff.pages.dealerportal.DPLoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinitions.ContextSteps;

public class dpHomePageSteps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public dpHomePageSteps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I am on home page for dealer portal")
	public void i_am_on_home_page_for_dealer_portal() throws IOException {
		DPLoginPage.using(driver).launchDP();
		DPLoginPage.using(driver).enterCredentials();
		DPLoginPage.using(driver).clickLoginButton();
		DPHomePage.using(driver).validatePageHeader();
		logger.info("Login to delaer portal is successful");
	}

	@When("I click on logout link")
	public void i_click_on_logout_link() throws IOException {
	    DPHomePage.using(driver).logoutUser();
	    logger.info("logout is attempted");
	}

	@Then("the logout is successful")
	public void the_logout_is_successful() throws IOException {
		DPLoginPage.using(driver).validatePageTitle();
		 logger.info("logout is successful");
	}
	
	@When("I click on submitapp menu link")
	public void i_click_on_submitapp_menu_link() throws IOException {
		DPHomePage.using(driver).navigateToSubmitAppPage();
		 logger.info("clicked on submitapp menu");
	}

	@Then("I see submitapp page is loaded successfully")
	public void i_see_submitapp_page_is_loaded_successfully() throws IOException {
		DPHomePage.using(driver).validateSubmitAppPageHeader();
		 logger.info("submitapp page is loaded successfully");    
	}
	
	@When("I click on reports menu link")
	public void i_click_on_reports_menu_link() throws IOException {
		DPHomePage.using(driver).navigateToReportsPage();
		logger.info("clicked on reports menu link");
	}

	@Then("I see reports page is loaded successfully")
	public void i_see_reports_page_is_loaded_successfully() throws IOException {
	    DPHomePage.using(driver).validateReportsPageHeader();
	    logger.info("reports page is loaded successfully");
	}

	@When("I click on calculator menu link")
	public void i_click_on_calculator_menu_link() throws IOException {
		DPHomePage.using(driver).navigateToCalculatorPage();
		logger.info("clicked on calculator menu link");
	}

	@Then("I see calculator page is loaded successfully")
	public void i_see_calculator_page_is_loaded_successfully() throws IOException {
		DPHomePage.using(driver).validateCalculatorPageHeader();
		logger.info("calculator page is loaded successfully");
	}

	@When("I click on training menu link")
	public void i_click_on_training_menu_link() throws IOException {
		DPHomePage.using(driver).navigateToTrainingPage();
		logger.info("clicked on training menu link");
	}

	@Then("I see training page is loaded successfully")
	public void i_see_training_page_is_loaded_successfully() throws IOException {
		DPHomePage.using(driver).validateTrainingPageHeader();
		logger.info("training page is loaded successfully");
	}

	@When("I click on livechat menu link")
	public void i_click_on_livechat_menu_link() throws IOException {
		DPHomePage.using(driver).navigateToLiveChatPage();
		logger.info("clicked on live chat menu link");
	}

	@Then("I see livechat is loaded successfully")
	public void i_see_livechat_is_loaded_successfully() throws IOException {
		DPHomePage.using(driver).validateLiveChatFrame();
		logger.info("live chat page is loaded successfully");
	}

	
	
	
	
	

}
