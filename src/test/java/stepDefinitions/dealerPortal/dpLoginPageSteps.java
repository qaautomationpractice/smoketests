package stepDefinitions.dealerPortal;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.aff.pages.dealerportal.DPHomePage;
import com.aff.pages.dealerportal.DPLoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinitions.ContextSteps;

public class dpLoginPageSteps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public dpLoginPageSteps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I am on the dealer portal login page")
	public void i_am_on_the_dealer_portal_login_page() throws IOException {
	   DPLoginPage.using(driver).launchDP();
	   logger.info("the dealer portal is successfully lauynched");
	   DPLoginPage.using(driver).validatePageTitle();
	}
	
	@When("I enter the credentials and click on submit for the dealer")
	public void i_enter_the_credentials_and_click_on_submit_for_the_dealer() throws IOException {
		DPLoginPage.using(driver).enterCredentials();
		DPLoginPage.using(driver).clickLoginButton();
		logger.info("the credentials are entered and submit is clicked");
		
	}

	@Then("the user is successfully logged into dealer portal")
	public void the_user_is_successfully_logged_into_dealer_portal() throws IOException {
	    DPHomePage.using(driver).validatePageHeader();
	}
	
	
	
	
	

}
