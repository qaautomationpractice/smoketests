package stepDefinitions.rentAdvanceUA1;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.rentadvanceua1.RentAdvanceUA1;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepDefinitions.ContextSteps;

public class raua1Steps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public raua1Steps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I launch RAUA1 application")
	public void i_launch_raua1_application() throws IOException {
		RentAdvanceUA1.using(driver).launchRAUA1();
		RentAdvanceUA1.using(driver).validatePageTitle();
		logger.info("the rent advance UA1 portal is successfully launched");
	   
	}

	@Then("I verify that the RAUA1 page is launched without issues")
	public void i_verify_that_the_raua1_page_is_launched_without_issues() throws IOException {
		RentAdvanceUA1.using(driver).validatePageIsLaunched();
		logger.info("An element on rent advance UA1 is validated with its text");
	    
	}

}
