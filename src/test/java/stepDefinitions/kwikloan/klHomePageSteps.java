package stepDefinitions.kwikloan;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.kwikloan.KLHomePage;
import com.aff.pages.kwikloan.KLLoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinitions.ContextSteps;

public class klHomePageSteps {
	
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public klHomePageSteps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I am on home page for KwikLoan portal")
	public void i_am_on_home_page_for_kwik_loan_portal() throws IOException {
		KLLoginPage.using(driver).launchKLPage();
		KLLoginPage.using(driver).enterCredetnials();
		KLLoginPage.using(driver).clickLoginButton();
		KLHomePage.using(driver).validatePageTitle();
		logger.info("the user is on home page for Kwikloan portal");
	}

	@When("I click on KwikLoan logout button")
	public void i_click_on_kwik_loan_logout_button() throws IOException {
		KLHomePage.using(driver).signoutUser();
		logger.info("the signout button is clicked on Kwikloan portal");
	}

	@Then("the logout is successful from KwikLoan")
	public void the_logout_is_successful_from_kwik_loan() throws IOException {
		KLLoginPage.using(driver).validatePageTitle();
		logger.info("the user is successfully signed out of Kwikloan portal");
	}
	

}
