package stepDefinitions.kwikloan;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.kwikloan.KLHomePage;
import com.aff.pages.kwikloan.KLLoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinitions.ContextSteps;

public class klLoginPageSteps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public klLoginPageSteps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	
	@Given("I am on the KwikLoan portal login page")
	public void i_am_on_the_kwik_loan_portal_login_page() throws IOException {
	   KLLoginPage.using(driver).launchKLPage();
	   logger.info("the Kwikloan portal is successfully launched");
	   KLLoginPage.using(driver).validatePageTitle();
	}

	@When("I enter the credentials and click on submit for the KwikLoan user")
	public void i_enter_the_credentials_and_click_on_submit_for_the_kwik_loan_user() throws IOException {
		KLLoginPage.using(driver).enterCredetnials();
		KLLoginPage.using(driver).clickLoginButton();
		logger.info("the user credentials are entered and submitted for Kwikloan");
		
		
	}

	@Then("the user is successfully logged into KwikLoan portal")
	public void the_user_is_successfully_logged_into_kwik_loan_portal() throws IOException {
		KLHomePage.using(driver).validatePageTitle();
		logger.info("the user is succssfully logged into Kwikloan portal");
	    
	}

}
