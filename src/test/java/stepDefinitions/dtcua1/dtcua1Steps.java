package stepDefinitions.dtcua1;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.dtcua1.DTCUA1;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepDefinitions.ContextSteps;

public class dtcua1Steps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public dtcua1Steps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I launch DTCUA1 application")
	public void i_launch_dtcua1_application() throws IOException {
		DTCUA1.using(driver).launchDTCUA1();
		DTCUA1.using(driver).validatePageTitle();
		logger.info("the DTC UA1 portal is successfully launched");
	}

	@Then("I verify that the dtcua1 page is launched without issues")
	public void i_verify_that_the_page_is_launched_without_issues() throws IOException {
		DTCUA1.using(driver).validatePageIsLaunched();
		logger.info("An element on DTC UA1 is validated with its text");
	    
	}
	

}
