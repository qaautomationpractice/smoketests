package stepDefinitions.virtualCardUA1;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aff.pages.virtualcardua1.VirtualCardUA1;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepDefinitions.ContextSteps;

public class vcua1Steps {
	WebDriver driver;
	private ContextSteps  steps;
	private Logger logger;
	public vcua1Steps(ContextSteps  steps) {
		this.steps=steps;
		this.logger = steps.getLogger();
		driver = steps.getDriver();
	}
	
	@Given("I launch VCUA1 application")
	public void i_launch_vcua1_application() throws IOException {
		VirtualCardUA1.using(driver).launchVCUA1();
		VirtualCardUA1.using(driver).validatePageTitle();
		logger.info("the virtual card UA1 portal is successfully launched");
	}

	@Then("I verify that the VCUA1 page is launched without issues")
	public void i_verify_that_the_vcua1_page_is_launched_without_issues() throws IOException {
		VirtualCardUA1.using(driver).validatePageIsLaunched();
		logger.info("An element on virtual card UA1 is validated with its text");
	   
	}

}
