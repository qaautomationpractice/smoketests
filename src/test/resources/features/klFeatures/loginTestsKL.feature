Feature: KwikLoan portal login test

@smoke
Scenario: Verify successful login
Given I am on the KwikLoan portal login page
When I enter the credentials and click on submit for the KwikLoan user
Then the user is successfully logged into KwikLoan portal

Scenario: Verify successful logout
Given I am on home page for KwikLoan portal
When I click on KwikLoan logout button
Then the logout is successful from KwikLoan