Feature: Consumer portal login test

@smoke
Scenario: Verify successful login
Given I am on the consumer portal login page
When I enter the credentials and click on submit for the customer user
Then the user is successfully logged into consumer portal

Scenario: Verify successful logout
Given I am on home page for consumer portal
When I click on consumer portal logout button
Then the logout is successful from consumer portal