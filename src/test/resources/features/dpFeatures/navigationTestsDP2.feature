Feature: Dealer portal navigation tests

@smoke
Scenario: Verify that submit app page is loaded without errors
Given I am on home page for dealer portal
When  I click on submitapp menu link
Then I see submitapp page is loaded successfully

@smoke
Scenario: Verify that reports page is loaded without errors
Given I am on home page for dealer portal
When I click on reports menu link
Then I see reports page is loaded successfully

@smoke
Scenario: Verify that calculator page is loaded without errors
Given I am on home page for dealer portal
When I click on calculator menu link
Then I see calculator page is loaded successfully

@smoke
Scenario: Verify that training page is loaded without errors
Given I am on home page for dealer portal
When I click on training menu link
Then I see training page is loaded successfully

@smoke
Scenario: Verify that live chat page is loaded without errors
Given I am on home page for dealer portal
When I click on livechat menu link
Then I see livechat is loaded successfully
