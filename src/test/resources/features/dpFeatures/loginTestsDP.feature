Feature: Dealer portal login test

@smoke
Scenario: Verify successful login
Given I am on the dealer portal login page
When I enter the credentials and click on submit for the dealer
Then the user is successfully logged into dealer portal

Scenario: Verify successful logout
Given I am on home page for dealer portal
When I click on logout link
Then the logout is successful